-- dummy table for testing purposes

CREATE SCHEMA IF NOT EXISTS test;

CREATE TABLE IF NOT EXISTS test.bicyclecounters_detections (
	locations_id varchar(255) NOT NULL,
	directions_id varchar(255) NOT NULL,
	measured_from timestamptz NOT NULL,
	measured_to timestamptz NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	value_pedestrians int4 NULL,
	CONSTRAINT test_bicyclecounters_detections_pkey_new PRIMARY KEY (locations_id,directions_id,measured_from)
);

CREATE TABLE IF NOT EXISTS test.bicyclecounters_directions (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	locations_id varchar(255) NOT NULL,
	"name" varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT test_bicyclecounters_directions_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS test.bicyclecounters_locations (
	id varchar(255) NOT NULL,
	vendor_id varchar(255) NOT NULL,
	lat float8 NOT NULL,
	lng float8 NOT NULL,
	"name" varchar(255) NULL,
	route varchar(255) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT test_bicyclecounters_locations_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS test.bicyclecounters_temperatures (
	locations_id varchar(255) NOT NULL,
	measured_from timestamptz NOT NULL,
	measured_to timestamptz NOT NULL,
	value int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT test_bicyclecounters_temperatures_pkey_new PRIMARY KEY (locations_id, measured_from)
);

-- EXAMPLE DATA - for testing purposes

INSERT INTO "test"."bicyclecounters_locations" ("id", "vendor_id", "lat", "lng", "name", "route", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by") VALUES
('camea-BC_VK-HRUP',	'BC_VK-HRUP',	50.089491724101,	14.460735619068,	'Drážní stezka - Vítkov',	'A 25',	NULL,	'2020-01-17 12:25:08.322+00',	NULL,	NULL,	'2020-03-22 14:50:01.167+00',	NULL),
('camea-BC_VS-CE',	'BC_VS-CE',	50.0704264,	14.3857008,	'Košíře',	'A 14',	NULL,	'2020-01-17 12:25:08.322+00',	NULL,	NULL,	'2020-03-22 14:50:01.167+00',	NULL),
('camea-BC_ZA-KLBO',	'BC_ZA-KLBO',	50.1433144,	14.3993322,	'V Zámcích',	'A2',	NULL,	'2020-01-17 12:25:08.322+00',	NULL,	NULL,	'2020-03-22 14:50:01.167+00',	NULL)
ON CONFLICT DO NOTHING;

INSERT INTO "test"."bicyclecounters_directions" ("id", "vendor_id", "locations_id", "name", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by") VALUES
('camea-BC_VK-HR',	'BC_VK-HR',	'camea-BC_VK-HRUP',	'Hrdlořezy',	NULL,	'2020-01-17 12:25:09.767+00',	NULL,	NULL,	'2020-03-22 14:50:01.398+00',	NULL),
('camea-BC_VK-UP',	'BC_VK-UP',	'camea-BC_VK-HRUP',	'U památníku',	NULL,	'2020-01-17 12:25:09.767+00',	NULL,	NULL,	'2020-03-22 14:50:01.398+00',	NULL),
('camea-BC_VS-CE',	'BC_VS-CE',	'camea-BC_VS-CE',	'centrum',	NULL,	'2020-01-17 12:25:09.767+00',	NULL,	NULL,	'2020-03-22 14:50:01.399+00',	NULL),
('camea-BC_ZA-KL',	'BC_ZA-KL',	'camea-BC_ZA-KLBO',	'Klecany',	NULL,	'2020-01-17 12:25:09.767+00',	NULL,	NULL,	'2020-03-22 14:50:01.399+00',	NULL),
('camea-BC_ZA-BO',	'BC_ZA-BO',	'camea-BC_ZA-KLBO',	'Bohnice',	NULL,	'2020-01-17 12:25:09.767+00',	NULL,	NULL,	'2020-03-22 14:50:01.399+00',	NULL)
ON CONFLICT DO NOTHING;

INSERT INTO "test"."bicyclecounters_detections" ("locations_id", "directions_id", "measured_from", "measured_to", "value", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by", "value_pedestrians") VALUES
('camea-BC_VK-HRUP',	'camea-BC_VK-HR',	to_timestamp(1584183300),	to_timestamp(1584183600),	4,	-1,	'2020-03-14 11:05:06.852411+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584183000),	to_timestamp(1584183300),	3,	-1,	'2020-03-14 11:00:27.856626+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	3),
('camea-BC_VK-HRUP',	'camea-BC_VK-UP',	to_timestamp(1584183000),	to_timestamp(1584183300),	4,	-1,	'2020-03-14 11:00:26.857761+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-BO',	to_timestamp(1584182700),	to_timestamp(1584183000),	5,	-1,	'2020-03-14 10:55:07.65846+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	NULL),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584182700),	to_timestamp(1584183000),	10,	-1,	'2020-03-14 10:55:07.65846+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584182400),	to_timestamp(1584182700),	3,	-1,	'2020-03-14 10:50:07.448117+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584182100),	to_timestamp(1584182400),	11,	-1,	'2020-03-14 10:45:07.338274+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584181800),	to_timestamp(1584182100),	6,	-1,	'2020-03-14 10:40:06.557032+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	7),
('camea-BC_VK-HRUP',	'camea-BC_VK-UP',	to_timestamp(1584181800),	to_timestamp(1584182100),	5,	-1,	'2020-03-14 10:40:05.777015+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	8),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584181200),	to_timestamp(1584181500),	4,	-1,	'2020-03-14 10:30:08.810805+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_VK-HRUP',	'camea-BC_VK-UP',	to_timestamp(1584181200),	to_timestamp(1584181500),	6,	-1,	'2020-03-14 10:30:07.938707+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	0),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584180900),	to_timestamp(1584181200),	4,	-1,	'2020-03-14 10:25:06.850141+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584180600),	to_timestamp(1584180900),	4,	-1,	'2020-03-14 10:20:06.842241+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-BO',	to_timestamp(1584180300),	to_timestamp(1584180600),	4,	-1,	'2020-03-14 10:15:07.775845+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584180300),	to_timestamp(1584180600),	6,	-1,	'2020-03-14 10:15:07.775845+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	4),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584180000),	to_timestamp(1584180300),	4,	-1,	'2020-03-14 10:10:07.418647+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_VK-HRUP',	'camea-BC_VK-UP',	to_timestamp(1584180000),	to_timestamp(1584180300),	3,	-1,	'2020-03-14 10:10:06.659036+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	7),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584179700),	to_timestamp(1584180000),	5,	-1,	'2020-03-14 10:05:07.293445+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584179400),	to_timestamp(1584179700),	4,	-1,	'2020-03-14 10:00:09.034003+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584179100),	to_timestamp(1584179400),	5,	-1,	'2020-03-14 09:55:06.595799+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584178800),	to_timestamp(1584179100),	6,	-1,	'2020-03-14 09:50:08.878222+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	0),
('camea-BC_VK-HRUP',	'camea-BC_VK-HR',	to_timestamp(1584178800),	to_timestamp(1584179100),	3,	-1,	'2020-03-14 09:50:08.235704+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	2),
('camea-BC_VK-HRUP',	'camea-BC_VK-UP',	to_timestamp(1584178800),	to_timestamp(1584179100),	3,	-1,	'2020-03-14 09:50:08.235704+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	0),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584178500),	to_timestamp(1584178800),	3,	-1,	'2020-03-14 09:45:07.985855+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-BO',	to_timestamp(1584178500),	to_timestamp(1584178800),	4,	-1,	'2020-03-14 09:45:07.985855+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	NULL),
('camea-BC_VK-HRUP',	'camea-BC_VK-UP',	to_timestamp(1584178500),	to_timestamp(1584178800),	3,	-1,	'2020-03-14 09:45:06.935666+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	1),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584178200),	to_timestamp(1584178500),	4,	-1,	'2020-03-14 09:40:07.268232+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	2),
('camea-BC_ZA-KLBO',	'camea-BC_ZA-KL',	to_timestamp(1584177900),	to_timestamp(1584178200),	3,	-1,	'2020-03-14 09:35:07.008885+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.650912+00',	'integration-engine',	0),
('camea-BC_VK-HRUP',	'camea-BC_VK-HR',	to_timestamp(1584177900),	to_timestamp(1584178200),	3,	-1,	'2020-03-14 09:35:06.531007+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.646195+00',	'integration-engine',	3)
ON CONFLICT DO NOTHING;

INSERT INTO "test"."bicyclecounters_temperatures" ("locations_id", "measured_from", "measured_to", "value", "create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by") VALUES
('camea-BC_VK-HRUP',	to_timestamp(1584177600),	to_timestamp(1584177900),	10,	-1,	'2020-03-14 09:30:09.629185+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584177900),	to_timestamp(1584178200),	10,	-1,	'2020-03-14 09:35:06.555745+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584178200),	to_timestamp(1584178500),	10,	-1,	'2020-03-14 09:40:06.616807+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584178500),	to_timestamp(1584178800),	10,	-1,	'2020-03-14 09:45:06.961899+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584178800),	to_timestamp(1584179100),	10,	-1,	'2020-03-14 09:50:08.255987+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584179100),	to_timestamp(1584179400),	10,	-1,	'2020-03-14 09:55:06.0094+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584179400),	to_timestamp(1584179700),	10,	-1,	'2020-03-14 10:00:08.42887+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584179700),	to_timestamp(1584180000),	10,	-1,	'2020-03-14 10:05:06.687607+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584180000),	to_timestamp(1584180300),	10,	-1,	'2020-03-14 10:10:06.682393+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584180300),	to_timestamp(1584180600),	11,	-1,	'2020-03-14 10:15:07.093541+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584180600),	to_timestamp(1584180900),	11,	-1,	'2020-03-14 10:20:06.062893+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584180900),	to_timestamp(1584181200),	11,	-1,	'2020-03-14 10:25:06.149189+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584181200),	to_timestamp(1584181500),	10,	-1,	'2020-03-14 10:30:07.980846+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584181500),	to_timestamp(1584181800),	10,	-1,	'2020-03-14 10:35:07.920386+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584181800),	to_timestamp(1584182100),	10,	-1,	'2020-03-14 10:40:05.805547+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584182100),	to_timestamp(1584182400),	10,	-1,	'2020-03-14 10:45:06.742474+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584182400),	to_timestamp(1584182700),	10,	-1,	'2020-03-14 10:50:06.341474+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584182700),	to_timestamp(1584183000),	11,	-1,	'2020-03-14 10:55:06.858758+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584183000),	to_timestamp(1584183300),	10,	-1,	'2020-03-14 11:00:26.877299+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_VK-HRUP',	to_timestamp(1584183300),	to_timestamp(1584183600),	11,	-1,	'2020-03-14 11:05:06.872869+00',	'integration-engine',	-1,	'2020-03-15 04:00:20.835272+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584178200),	to_timestamp(1584178500),	10,	-1,	'2020-03-14 09:40:07.290585+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584178500),	to_timestamp(1584178800),	10,	-1,	'2020-03-14 09:45:08.015288+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584179400),	to_timestamp(1584179700),	10,	-1,	'2020-03-14 10:00:09.070265+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584179700),	to_timestamp(1584180000),	10,	-1,	'2020-03-14 10:05:07.319131+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584180000),	to_timestamp(1584180300),	10,	-1,	'2020-03-14 10:10:07.440586+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584180300),	to_timestamp(1584180600),	10,	-1,	'2020-03-14 10:15:07.797602+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584180600),	to_timestamp(1584180900),	10,	-1,	'2020-03-14 10:20:06.882923+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584180900),	to_timestamp(1584181200),	10,	-1,	'2020-03-14 10:25:07.099943+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584181200),	to_timestamp(1584181500),	11,	-1,	'2020-03-14 10:30:08.835391+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584181500),	to_timestamp(1584181800),	11,	-1,	'2020-03-14 10:35:08.573236+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584181800),	to_timestamp(1584182100),	11,	-1,	'2020-03-14 10:40:06.581227+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584182100),	to_timestamp(1584182400),	11,	-1,	'2020-03-14 10:45:07.361461+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584182400),	to_timestamp(1584182700),	11,	-1,	'2020-03-14 10:50:07.481585+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584182700),	to_timestamp(1584183000),	10,	-1,	'2020-03-14 10:55:07.679932+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584183000),	to_timestamp(1584183300),	11,	-1,	'2020-03-14 11:00:27.87918+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine'),
('camea-BC_ZA-KLBO',	to_timestamp(1584183300),	to_timestamp(1584183600),	11,	-1,	'2020-03-14 11:05:07.83522+00',	'integration-engine',	-1,	'2020-03-15 04:00:21.740315+00',	'integration-engine')
ON CONFLICT DO NOTHING;
