import bodyParser from "body-parser";
import { format as fastCsvFormat, CsvFormatterStream } from "fast-csv";
import { Field, RuleGroupType } from "react-querybuilder";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { ExportingModuleModel, IQuerySchema, ITableSchema } from "./ExportingModuleModel";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ContainerToken, OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc";
import { dateTime } from "@golemio/core/dist/helpers";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";

export interface IInputSchema {
    where: string;
    table_schema: string;
    column_name: string;
    is_nullable: string;
    data_type: string;
}

export interface IRequestBody {
    builderQuery: RuleGroupType;
    columns: string[];
    groupBy: string[];
    limit: number;
    offset: number;
    order: Array<{
        direction: string;
        column: string;
    }>;
}

export class ExportingModuleRouter extends BaseRouter {
    private cacheHeaderMiddleware: CacheHeaderMiddleware;
    private maxAge = 60 * 60; // 60 minutes
    private staleWhileRevalidate = 60; // 1 minute

    public router: Router = Router();

    protected exportingModuleModel: ExportingModuleModel = new ExportingModuleModel();

    private auditFields = ["create_batch_id", "created_at", "created_by", "update_batch_id", "updated_at", "updated_by"];

    public constructor() {
        super();
        this.cacheHeaderMiddleware = OutputGatewayContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);
        this.initRoutes();
    }

    public getAll = async (req: Request, res: Response, next: NextFunction) => {
        let body: IRequestBody;

        try {
            if (typeof req.body === "string") {
                body = JSON.parse(req.body);
            } else {
                body = req.body;
            }

            this.checkBody(body);
        } catch (err) {
            throw new GeneralError("Getting All Request body is not valid.", "exporting-module", err.message, 400);
        }

        try {
            // maybe should be stream - we'll see
            const data = await this.getData({
                builderQuery: body.builderQuery,
                columns: body.columns,
                groupBy: body.groupBy,
                limit: body.limit,
                offset: body.offset,
                order: body.order,
                table: req.params.resource,
                schema: req.params.schema,
            });

            res.set("Content-Type", "application/octet-stream");
            res.attachment(`${req.params.resource}.csv`);

            if (Array.isArray(data) && data.length > 0) {
                const csvStream = this.initCsvFormatterStream();
                csvStream.pipe(res);
                for (const d of data) {
                    csvStream.write(d);
                }
                csvStream.end();
            } else {
                res.send("");
            }
        } catch (err) {
            throw new GeneralError("Error while getting All data.", "exporting-module", err.message, 400);
        }
    };

    public getMetaData = async (req: Request, res: Response, next: NextFunction) => {
        const tableName: string = req.params.resource;
        const schema: string = req.params.schema;
        try {
            const data = await this.exportingModuleModel.getTableMetadata(tableName, schema);
            res.setHeader("content-type", "application/json; charset=utf-8");
            res.status(200).send(this.transformMeta(data));
        } catch (err) {
            return next(err);
        }
    };

    public getPreview = async (req: Request, res: Response, next: NextFunction) => {
        let body: IRequestBody;

        try {
            if (typeof req.body === "string") {
                body = JSON.parse(req.body);
            } else {
                body = req.body;
            }

            this.checkBody(body);
        } catch (err) {
            throw new GeneralError("Getting Preview Request body is not valid..", "exporting-module", err.message, 400);
        }

        try {
            const data = await this.getData({
                builderQuery: body.builderQuery,
                columns: body.columns,
                groupBy: body.groupBy,
                limit: 20,
                offset: body.offset,
                order: body.order,
                table: req.params.resource,
                schema: req.params.schema,
            });

            res.setHeader("content-type", "text/plain; charset=utf-8");

            if (Array.isArray(data) && data.length > 0) {
                res.status(200);
                const csvStream = this.initCsvFormatterStream();
                csvStream.pipe(res);
                for (const d of data) {
                    csvStream.write(d);
                }
                csvStream.end();
            } else {
                return res.status(200).send("");
            }
        } catch (err) {
            throw new GeneralError("Error while getting Preview.", "exporting-module", err.message, 400);
        }
    };

    private checkBody = (body: any) => {
        if (body) {
            if (!body.builderQuery) {
                throw new Error("builderQuery parameter is required");
            }
            if (!body.columns) {
                throw new Error("columns parameter is required");
            }
        }
    };

    private catchJsonError = (error: any, req: any, res: any, next: any) => {
        if (error instanceof SyntaxError) {
            return res.status(400).send(`Invalid input: ${error}`);
        } else {
            next();
        }
    };

    private initRoutes = (): void => {
        this.router.post("/:resource/preview/:schema", bodyParser.json(), this.catchJsonError, this.getPreview);

        this.router.post("/:resource/data", bodyParser.json(), this.catchJsonError, this.getAll);
        this.router.post("/:resource/data/:schema", bodyParser.json(), this.catchJsonError, this.getAll);

        this.router.get(
            "/:resource/meta",
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.getMetaData
        );
        this.router.get(
            "/:resource/meta/:schema",
            this.cacheHeaderMiddleware.getMiddleware(this.maxAge, this.staleWhileRevalidate),
            this.getMetaData
        );
    };

    private getData = async (options: IQuerySchema) => {
        return await this.exportingModuleModel.getData({
            builderQuery: options.builderQuery,
            columns: options.columns,
            groupBy: options.groupBy,
            limit: options.limit,
            offset: options.offset,
            order: options.order,
            table: options.table,
            schema: options.schema,
        });
    };

    private transformMeta = (data: ITableSchema[]): Field[] => {
        const out: Field[] = [];
        data.forEach((field: ITableSchema) => {
            if (!this.auditFields.includes(field.column_name)) {
                /* eslint-disable max-len */
                out.push({
                    name: field.column_name, // REQUIRED - the field name
                    label: field.column_name, // REQUIRED - the field label
                    // operators: { name: string; label: string; }[]; // Array of operators (if not provided, then `getOperators()` will be used)
                    valueEditorType: this.getEditorType(field.data_type).type, // 'text' | 'select' | 'checkbox' | 'radio' | null; // Value editor type for this field (if not provided, then `getValueEditorType()` will be used)
                    inputType: this.getEditorType(field.data_type).inputType,
                    // values: { name: string; label: string; }[]; // Array of values, applicable when valueEditorType is 'select' or 'radio' (if not provided, then `getValues()` will be used)
                    // defaultValue?: any; // Default value for this field (if not provided, then `getDefaultValue()` will be used)
                    // placeholder?: string; // Value to be displayed in the placeholder of the text field
                });
                /* eslint-enable max-len */
            }
        });
        return out;
    };

    private getEditorType = (inputType: string): any => {
        switch (inputType) {
            case "boolean":
            case "bool":
                return {
                    type: "checkbox",
                };
            case "bigint":
            case "integer":
            case "real":
            case "smallint":
                return {
                    inputType: "number",
                    type: "text",
                };
            default:
                return {
                    inputType: "text",
                    type: "text",
                };
        }
    };

    private initCsvFormatterStream = (): CsvFormatterStream<{ [key: string]: any }, { [key: string]: any }> => {
        const csvStream = fastCsvFormat({
            headers: true,
            quoteColumns: true,
            transform: (row: { [key: string]: any }) => {
                for (const property in row) {
                    if (typeof row[property] === "object" && row[property] instanceof Date) {
                        row[property] = dateTime(row[property]).setTimeZone("Europe/Prague").toISOString();
                    }
                }
                return row;
            },
        });
        return csvStream;
    };
}

const exportingModuleRouter: Router = new ExportingModuleRouter().router;

export { exportingModuleRouter };
