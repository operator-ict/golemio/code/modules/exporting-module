# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Removed

-   remove exporting module ([general#625](https://gitlab.com/operator-ict/golemio/code/general/-/issues/625))

## [1.1.13] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.1.12] - 2024-08-20

### Added

-   add cache-control headers ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))
-   add backstage metadata files
-   add .gitattributes file

### Removed

-   remove redis CacheMiddleware ([core#110](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/110))

## [1.1.11] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.10] - 2023-10-25

-   No changelog

## [1.1.9] - 2023-10-18

### Fixed

-   tests after clean up of schema definitions

## [1.1.7] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.1.6] - 2023-08-30

-   No changelog

## [1.1.5] - 2023-07-31

### Changed

-   Use DateTime wrapper instead of moment.js ([core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))

## [1.1.4] - 2023-06-26

### Changed

-   Refactor postgres connector ([core#17](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/17))

## [1.1.3] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.2] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.9] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.8] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.7] - 2022-06-30

### Fixed

-   Extended error response from text to json format ([OG#216](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/216))

## [1.0.6] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.5] - 2022-03-01

### Added

-   Api doc ([exporting-module#2](https://gitlab.com/operator-ict/golemio/code/modules/exporting-module/-/issues/2))

## [1.0.4] - 2021-12-14

### Added

-   take the schema as an optional request parameter ([OG#212](https://gitlab.com/operator-ict/golemio/code/output-gateway/-/issues/212))

### Fixed

-   Avoid the tests dependence on external data ([exporting-module#1](https://gitlab.com/operator-ict/golemio/code/modules/exporting-module/-/issues/1))
