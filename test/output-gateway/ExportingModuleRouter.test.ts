import { OutputGatewayContainer } from "@golemio/core/dist/output-gateway/ioc/Di";
import request from "supertest";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { exportingModuleRouter } from "#og/ExportingModuleRouter";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc/ContainerToken";

chai.use(chaiAsPromised);

describe("ExportingModuleRouter", () => {
    // Create clean express instance
    const app = express();

    before(() => {
        OutputGatewayContainer.resolve<IDatabaseConnector>(ContainerToken.PostgresDatabase).connect();
        // Mount the tested router to the express instance
        app.use("/export", exportingModuleRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    // TODO: fix tests
    it("should respond with correctly to GET /bicyclecounters_detections/meta", () => {
        return request(app)
            .get("/export/bicyclecounters_detections/meta/test")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                chai.expect(response.headers["cache-control"]).to.eq("public, s-maxage=3600, stale-while-revalidate=60");
                chai.expect(response.body.sort((a: any, b: any) => (a.name > b.name ? 1 : -1))).to.be.deep.equal(
                    [
                        {
                            name: "locations_id",
                            label: "locations_id",
                            valueEditorType: "text",
                            inputType: "text",
                        },
                        {
                            name: "directions_id",
                            label: "directions_id",
                            valueEditorType: "text",
                            inputType: "text",
                        },
                        {
                            name: "measured_from",
                            label: "measured_from",
                            valueEditorType: "text",
                            inputType: "text",
                        },
                        {
                            name: "measured_to",
                            label: "measured_to",
                            valueEditorType: "text",
                            inputType: "text",
                        },
                        {
                            name: "value",
                            label: "value",
                            valueEditorType: "text",
                            inputType: "number",
                        },
                        {
                            name: "value_pedestrians",
                            label: "value_pedestrians",
                            valueEditorType: "text",
                            inputType: "number",
                        },
                    ].sort((a: any, b: any) => (a.name > b.name ? 1 : -1))
                );
            });
    });

    it("should respond correctly to GET /export/bicyclecounters_detections/preview", (done) => {
        request(app)
            .post("/export/bicyclecounters_detections/preview/test")
            .send({
                columns: ["locations_id", "directions_id", "measured_from", "measured_to", "value"],
                order: [
                    {
                        direction: "asc",
                        column: "locations_id",
                    },
                    {
                        direction: "asc",
                        column: "directions_id",
                    },
                    {
                        direction: "asc",
                        column: "measured_from",
                    },
                    {
                        direction: "asc",
                        column: "measured_to",
                    },
                    {
                        direction: "asc",
                        column: "value",
                    },
                ],
                offset: 1,
                builderQuery: {
                    combinator: "and",
                    not: false,
                    rules: [
                        {
                            field: "locations_id",
                            operator: "notNull",
                            value: "",
                        },
                        {
                            combinator: "and",
                            rules: [
                                {
                                    field: "measured_to",
                                    operator: ">",
                                    value: "2020-03-14 10:00:00.000 +0100",
                                },
                            ],
                        },
                    ],
                },
            })
            .set("Accept", "text/plain")
            .expect("Content-Type", /text/)
            .expect(
                200,
                '"locations_id","directions_id","measured_from","measured_to","value"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-HR","2020-03-14T10:40:00+01:00","2020-03-14T10:45:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-HR","2020-03-14T11:55:00+01:00","2020-03-14T12:00:00+01:00","4"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T10:35:00+01:00","2020-03-14T10:40:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T10:40:00+01:00","2020-03-14T10:45:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:00:00+01:00","2020-03-14T11:05:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:20:00+01:00","2020-03-14T11:25:00+01:00","6"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:30:00+01:00","2020-03-14T11:35:00+01:00","5"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:50:00+01:00","2020-03-14T11:55:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-BO","2020-03-14T10:35:00+01:00","2020-03-14T10:40:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-BO","2020-03-14T11:05:00+01:00","2020-03-14T11:10:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-BO","2020-03-14T11:45:00+01:00","2020-03-14T11:50:00+01:00","5"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:25:00+01:00","2020-03-14T10:30:00+01:00","3"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:30:00+01:00","2020-03-14T10:35:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:35:00+01:00","2020-03-14T10:40:00+01:00","3"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:40:00+01:00","2020-03-14T10:45:00+01:00","6"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:45:00+01:00","2020-03-14T10:50:00+01:00","5"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:50:00+01:00","2020-03-14T10:55:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:55:00+01:00","2020-03-14T11:00:00+01:00","5"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:00:00+01:00","2020-03-14T11:05:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:05:00+01:00","2020-03-14T11:10:00+01:00","6"',
                done
            );
    });

    it("should respond correctly to GET /export/bicyclecounters_detections/data", (done) => {
        request(app)
            .post("/export/bicyclecounters_detections/data/test")
            .send({
                columns: ["locations_id", "directions_id", "measured_from", "measured_to", "value"],
                order: [
                    {
                        direction: "asc",
                        column: "locations_id",
                    },
                    {
                        direction: "asc",
                        column: "directions_id",
                    },
                    {
                        direction: "asc",
                        column: "measured_from",
                    },
                    {
                        direction: "asc",
                        column: "measured_to",
                    },
                    {
                        direction: "asc",
                        column: "value",
                    },
                ],

                limit: 50,
                offset: 1,
                builderQuery: {
                    combinator: "and",
                    not: false,
                    rules: [
                        {
                            field: "locations_id",
                            operator: "notNull",
                            value: "",
                        },
                        {
                            combinator: "and",
                            rules: [
                                {
                                    field: "measured_to",
                                    operator: ">",
                                    value: "2020-03-14 10:00:00.000 +0100",
                                },
                            ],
                        },
                    ],
                },
            })
            .expect(
                200,
                '"locations_id","directions_id","measured_from","measured_to","value"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-HR","2020-03-14T10:40:00+01:00","2020-03-14T10:45:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-HR","2020-03-14T11:55:00+01:00","2020-03-14T12:00:00+01:00","4"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T10:35:00+01:00","2020-03-14T10:40:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T10:40:00+01:00","2020-03-14T10:45:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:00:00+01:00","2020-03-14T11:05:00+01:00","3"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:20:00+01:00","2020-03-14T11:25:00+01:00","6"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:30:00+01:00","2020-03-14T11:35:00+01:00","5"\n' +
                    '"camea-BC_VK-HRUP","camea-BC_VK-UP","2020-03-14T11:50:00+01:00","2020-03-14T11:55:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-BO","2020-03-14T10:35:00+01:00","2020-03-14T10:40:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-BO","2020-03-14T11:05:00+01:00","2020-03-14T11:10:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-BO","2020-03-14T11:45:00+01:00","2020-03-14T11:50:00+01:00","5"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:25:00+01:00","2020-03-14T10:30:00+01:00","3"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:30:00+01:00","2020-03-14T10:35:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:35:00+01:00","2020-03-14T10:40:00+01:00","3"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:40:00+01:00","2020-03-14T10:45:00+01:00","6"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:45:00+01:00","2020-03-14T10:50:00+01:00","5"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:50:00+01:00","2020-03-14T10:55:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T10:55:00+01:00","2020-03-14T11:00:00+01:00","5"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:00:00+01:00","2020-03-14T11:05:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:05:00+01:00","2020-03-14T11:10:00+01:00","6"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:10:00+01:00","2020-03-14T11:15:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:15:00+01:00","2020-03-14T11:20:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:20:00+01:00","2020-03-14T11:25:00+01:00","4"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:30:00+01:00","2020-03-14T11:35:00+01:00","6"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:35:00+01:00","2020-03-14T11:40:00+01:00","11"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:40:00+01:00","2020-03-14T11:45:00+01:00","3"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:45:00+01:00","2020-03-14T11:50:00+01:00","10"\n' +
                    '"camea-BC_ZA-KLBO","camea-BC_ZA-KL","2020-03-14T11:50:00+01:00","2020-03-14T11:55:00+01:00","3"',
                done
            );
    });

    it("should respond correctly to GET /export/bicyclecounters_detections/data with groupBy", (done) => {
        request(app)
            .post("/export/bicyclecounters_detections/data/test")
            .send({
                columns: ["measured_from", "count(value) as value"],
                order: [
                    {
                        direction: "asc",
                        column: "measured_from",
                    },
                    {
                        direction: "asc",
                        column: "value",
                    },
                ],
                groupBy: ["measured_from", "value"],
                limit: 50,
                offset: 1,
                builderQuery: {
                    combinator: "and",
                    not: false,
                    rules: [
                        {
                            field: "measured_from",
                            operator: ">",
                            value: "2020-03-14 10:00:00.000 +0100",
                        },
                    ],
                },
            })
            .expect(
                200,
                '"measured_from","value"\n' +
                    '"2020-03-14T10:30:00+01:00","1"\n' +
                    '"2020-03-14T10:35:00+01:00","1"\n' +
                    '"2020-03-14T10:35:00+01:00","2"\n' +
                    '"2020-03-14T10:40:00+01:00","1"\n' +
                    '"2020-03-14T10:40:00+01:00","2"\n' +
                    '"2020-03-14T10:45:00+01:00","1"\n' +
                    '"2020-03-14T10:50:00+01:00","1"\n' +
                    '"2020-03-14T10:55:00+01:00","1"\n' +
                    '"2020-03-14T11:00:00+01:00","1"\n' +
                    '"2020-03-14T11:00:00+01:00","1"\n' +
                    '"2020-03-14T11:05:00+01:00","1"\n' +
                    '"2020-03-14T11:05:00+01:00","1"\n' +
                    '"2020-03-14T11:10:00+01:00","1"\n' +
                    '"2020-03-14T11:15:00+01:00","1"\n' +
                    '"2020-03-14T11:20:00+01:00","1"\n' +
                    '"2020-03-14T11:20:00+01:00","1"\n' +
                    '"2020-03-14T11:30:00+01:00","1"\n' +
                    '"2020-03-14T11:30:00+01:00","1"\n' +
                    '"2020-03-14T11:35:00+01:00","1"\n' +
                    '"2020-03-14T11:40:00+01:00","1"\n' +
                    '"2020-03-14T11:45:00+01:00","1"\n' +
                    '"2020-03-14T11:45:00+01:00","1"\n' +
                    '"2020-03-14T11:50:00+01:00","1"\n' +
                    '"2020-03-14T11:50:00+01:00","1"\n' +
                    '"2020-03-14T11:55:00+01:00","1"',
                done
            );
    });
});
